import logo from './logo.svg';
import TableExample from './TableExample';
import Bird from './Bird';


function App() {
  return (
    <div className="App">
      <Bird/>
    </div>
  );
}

export default App;
