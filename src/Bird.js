import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import birds from './bird.json';
import { Typography } from '@mui/material';

export default function Bird()
{    

    // Sort data
    birds.sort((a, b) => (a.finnish > b.finnish) ? 1 : -1);

    return(
        <div>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead sx={{backgroundColor: 'lightgray'}}>
                    <TableRow>
                        <TableCell><Typography fontWeight="bold">Finnish</Typography></TableCell>
                        <TableCell align="left"><Typography fontWeight="bold">Swedish</Typography></TableCell>
                        <TableCell align="left"><Typography fontWeight="bold">English</Typography></TableCell>
                        <TableCell align="left"><Typography fontWeight="bold">Short</Typography></TableCell>
                        <TableCell align="left"><Typography fontWeight="bold">Latin</Typography></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {birds.map((row) => (
                        <TableRow
                        key={row.finnish}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                        <TableCell component="th" scope="row">
                            {row.finnish}
                        </TableCell>
                        <TableCell align="left">{row.swedish}</TableCell>
                        <TableCell align="left">{row.english}</TableCell>
                        <TableCell align="left">{row.short}</TableCell>
                        <TableCell align="left">{row.latin}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </TableContainer>
        </div>
    );
}